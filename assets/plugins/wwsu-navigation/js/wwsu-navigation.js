"use strict";

// This class manages the sidebar navigation menu on WWSU pages using AdminLTE
// Supports popState event, fired whenever window.popstate is and e.state exists for historical data.

class WWSUNavigation extends WWSUevents {
  /**
   * Construct the class
   */
  constructor(manager, options) {
    super();
    this.manager = manager;
    this.elements = [];
    this.activeMenu = ``;

    this.pushStateFns = new Map(); // Add functions (which return objects) here from other scripts that will add to the pushState data object for navigation.

    window.onpopstate = (e) => {
      if (e.state) {
        this.emitEvent("popState", [e.state]);
        this.processMenu(
          e.state.dom,
          e.state.e ? document.getElementById(e.state.e) : undefined,
          true
        );
        document.title = e.state.pageTitle;
      }
    };
  }

  /**
   * Add a menu item to the controller
   *
   * @param {string} dom DOM query string for the navigation item; should only return one!
   * @param {string} section DOM query string for the section that should be visible when this menu is active, and invisible when it is not. Should only return one!
   * @param {string|function} url Relative URL for this menu item. Can be a function that returns a string (clicked element is parameter).
   * @param {boolean} defaultItem Set to true to make this the default menu activated. Defaults to false
   * @param {function} callback Function called when this menu becomes active. Passes (e {Element}, popState {boolean}) where e is the element that was clicked / triggered the menu, and popState is true if menu was activated from window.onpopstate (eg. navigation)
   */
  addItem(dom, section, title, url, defaultItem = false, callback = () => {}) {
    // Remove item if it already exists
    this.elements = this.elements.filter((ele) => ele.dom !== dom);

    this.elements.push({ dom, section, title, url, callback });

    if (defaultItem) {
      $(dom).addClass("active");
      $(section).removeClass("d-none");
      callback();
    } else {
      $(dom).removeClass("active");
      $(section).addClass("d-none");
    }

    $(dom).unbind("click");
    $(dom).click((e) => {
      this.processMenu(dom, e.currentTarget);
    });

    $(dom).unbind("keypress");
    $(dom).keypress((e) => {
      if (e.which === 13) {
        this.processMenu(dom, e.currentTarget);
      }
    });

    if (this.activeMenu === dom) {
      this.processMenu(dom);
    }

    return this;
  }

  /**
   * Remove the provided nav element from the WWSUnavigation manager.
   *
   * @param {string} dom The DOM query string that was provided in addItem for this element to be removed.
   */
  removeItem(dom) {
    this.elements = this.elements.filter((ele) => ele.dom !== dom);
    return this;
  }

  /**
   * Activate the menu page provided.
   *
   * @param {string} dom DOM query string of the sidebar menu item to activate.
   * @param {object} e DOM element clicked to activate the menu, if applicable.
   * @param {boolean} popState Was this process a result of popstate (eg. back button)? Will be passed as a parameter to the menu's addItem callback when clicked.
   */
  processMenu(dom, e, popState = false) {
    this.activeMenu = dom;

    // De-activate all menu items and hide all their sections
    this.elements.forEach((element) => {
      $(element.dom).removeClass("active");
      $(element.section).addClass("d-none");

      // For breadcrumbs, convert their text to a link
      $(element.dom).each((i, el) => {
        if ($(el).hasClass("breadcrumb-item")) {
          $(el).html(`<a href="#">${$(el).html()}</a>`);
        }
      });
    });

    // Activate the menu item passed as dom parameter, and un-hide its section.
    this.elements
      .filter((element) => element.dom === dom)
      .map((element) => {
        $(element.dom).addClass("active");
        $(element.section).removeClass("d-none");

        // For breadcrumbs, convert their link to text
        $(element.dom).each((i, el) => {
          if ($(el).hasClass("breadcrumb-item")) {
            $(el).html($(el).find("a").first().html());
          }
        });

        // Add window history manually since we are not physically going to another page.
        if (!popState) {
          try {
            let pageTitle =
              typeof element.title === "function"
                ? element.title(e)
                : element.title;

            let pageURL =
              typeof element.url === "function" ? element.url(e) : element.url;

            // Run functions from other modules for adding data to the push state.
            let dataObject = {
              dom: element.dom,
              e: e ? e.id || undefined : undefined,
              pageTitle: pageTitle,
            };
            this.pushStateFns.forEach((fn) => {
              dataObject = Object.assign(dataObject, fn());
            });

            // Replace current history with this one
            window.history.replaceState(dataObject, pageTitle, pageURL);

            document.title = pageTitle;
          } catch (e) {
            console.error(e);
          }
        }

        // Trigger callback
        element.callback(e, popState);
      });

    // Manually trigger resize event
    window.requestAnimationFrame(() => {
      window.dispatchEvent(new Event("resize"));
    });
  }
}
